/*
const functions = require('firebase-functions');
const admin = require('firebase-admin');
// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions


admin.initializeApp(functions.config().firebase);
*/
/*
 * Trigger Name - includeAssignedRoomCounts
 * Trigger Description - This modifies a key named assigned_rooms_count which tracks how many rooms a User has been assigned to
 * Object(s) Watched - Hotel.Room
 * Object(s) Modified - Hotel.User
 * * Keys Modified - Hotel.User.assigned_rooms_count
 * Objects(s) Accessed - Hotel.Room, Hotel.User
 */

/*
exports.includeAssignedRoomCounts = functions.database.ref('/hotels/{hotelId}/rooms/{roomNumber}').onWrite(function(event) {
    const newData = event.data;
    const prevData = event.data.previous;

    // Only edit data when it is already existing
    if (!prevData.exists()) {
        return;
    }
    // Exit when the data is deleted.
    if (!newData.exists()) {
        return;
    }

    //If the data has not changed
    if(!event.data.changed()) {
        return;
    }

    // The next to if-blocks assume that a room cannot switch owners and must be unassigned before they are assigned to somebody
    if(!prevData.val().assigned_to && newData.val().assigned_to) { // If the room has been assigned

        const maidRef = admin.database().ref('/hotels/'+ event.params.hotelId + '/users/' + newData.val().assigned_to + "/assigned_rooms_count");

        return maidRef.transaction(function(maidData) {
            //UNCOMMENT FOR TESTING
            //console.log("Room " + newData.val().room_number + " has been ASSIGNED");
            //console.log("Room " + newData.val().room_number + " - " + newData.val().assigned_maid_name + " has room count: " + ((maidData || 0) + 1));
            return (maidData || 0) + 1;
        }).then(function() {
            //This fixes the serializing return value error
            return null;
        });
    }

    // This condition needs to be done because the includeUserInformation triggers an additional save
    // which means this whole functions gets triggered again as well and numbers reset
    if(prevData.val().assigned_to && !newData.val().assigned_to) { // If the room has been unassigned

        const maidRef = admin.database().ref('/hotels/'+ event.params.hotelId + '/users/' + prevData.val().assigned_to + "/assigned_rooms_count");

        return maidRef.transaction(function(maidData) {
            //UNCOMMENT FOR TESTING
            //console.log("Room " + newData.val().room_number + " has been UNASSIGNED");
            //console.log("Room " + newData.val().room_number + " - " + prevData.val().assigned_maid_name + " has room count: " + ((maidData || 0) - 1));
            return (maidData || 0) - 1;
        }).then(function() {
            return null;
        });
});
*/