describe('FILTER hasRoomsAssigned', function() {

  beforeEach(module('lodgebookApp'));

  var $filter;

  beforeEach(inject(function(_$filter_){
    $filter = _$filter_;
  }));

  it('returns empty array when given null', function() {
    var hasRoomsAssigned = $filter('hasRoomsAssigned');
    expect(hasRoomsAssigned(null)).toEqual([]);
  });

  it('returns only users with assigned rooms greater than 0', function() {
    var hasRoomsAssigned = $filter('hasRoomsAssigned');
    var userArray = [
      {
      "assigned_rooms_count" : 0,
      "name" : "Tyler Terrace",
      "role" : "maid"
      },
      {
        "assigned_rooms_count" : 0,
        "name" : "Jane Joiner",
        "role" : "maid"
      },
      {
        "name" : "Martha More",
        "role" : "maid-supervisor"
      },
      {
        "name" : "Donna Door",
        "role" : "manager"
      },
      {
        "assigned_rooms_count" : 2,
        "name" : "Liz Litmus",
        "role" : "maid"
      },
      {
        "assigned_rooms_count" : 7,
        "name" : "Paul Practical",
        "role" : "maid"
      }];

    expect(hasRoomsAssigned(userArray)).toEqual([
      {
        "assigned_rooms_count" : 2,
        "name" : "Liz Litmus",
        "role" : "maid"
      },
      {
        "assigned_rooms_count" : 7,
        "name" : "Paul Practical",
        "role" : "maid"
      }]);
  })

});