/**
 * Created by Christopher on 4/29/17.
 */
'use strict';

(function() {
  var app = angular.module('lodgebookApp');

  app.directive('checkboxStackItem', [function() {
    return {
      restrict: 'E',
      require: 'ngModel',
      templateUrl: './directives/checkbox-stack-item/checkbox-stack-item.directive.html',
      scope: {
        title: '=',
        description: '=?',
        imageSrc: '=?'
      },
      link: function (scope, element, attrs, ngModel) {

        scope.$watch('checked', function(newVal, oldVal) {
          if(oldVal!==newVal) {
            ngModel.$setViewValue(newVal);
            ngModel.$render();
          }
        });

        ngModel.$render = function() {
          if(ngModel.$viewValue === true) {
            scope.checked = true;
          } else {
            scope.checked = false;
          }
        }

      }
    };
  }]);
})();