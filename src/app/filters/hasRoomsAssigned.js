'use strict';

(function() {
  var app = angular.module('lodgebookApp');

  app.filter('hasRoomsAssigned', function() {
    return function(maids) {
      var toReturn = [];

      // This check needs to be done because sometimes the data will not load before Angular does, meaning null or undefined values are passed to this filter
      if(!maids) {
        return toReturn;
      }

      for(var c=0; c<maids.length; c++) {
        if(maids[c].assigned_rooms_count && maids[c].assigned_rooms_count > 0) {
          toReturn.push(maids[c]);
        }
      }
      return toReturn;
    }
  });

})();