// TODO Configure this to be aware of its environment and use the correct environment variables

(function(){

  var firebaseEnv = null;

  var lodgebookDEV = {
    apiKey: "AIzaSyC4EXcoJNRPPFFh19QtaWkcBXccvTKAax0",
    authDomain: "lodgebook-281ad.firebaseapp.com",
    databaseURL: "https://lodgebook-281ad.firebaseio.com",
    storageBucket: "lodgebook-281ad.appspot.com",
    messagingSenderId: "1014556242585"
  };

  var lodgebookQA = {
    apiKey: "AIzaSyDSS74PLSFLbz2k9PWNGpbXDYvfKqKJB0k",
    authDomain: "lodgebook-qa.firebaseapp.com",
    databaseURL: "https://lodgebook-qa.firebaseio.com",
    projectId: "lodgebook-qa",
    storageBucket: "lodgebook-qa.appspot.com",
    messagingSenderId: "54648757662"
  };

  var lodgebookPROD = {

  };


  if(location.host.indexOf('localhost') === 0) {
    firebaseEnv = lodgebookDEV;
  } else if (location.host.indexOf('dev.mylodgebook.com') === 0) {
    firebaseEnv = lodgebookDEV;
  } else if (location.host.indexOf('qa.mylodgebook.com') === 0) {
    firebaseEnv = lodgebookQA;
  }

  firebase.initializeApp(firebaseEnv);
})();