'use strict';

(function() {
    var app = angular.module('lodgebookApp');

    app.controller('loginController', ['$scope', '$stateParams', '$firebaseObject','$firebaseArray','userService','$state', function($scope, $stateParams,$firebaseObject,$firebaseArray,userService,$state) {
        console.log('login controller accessed');
        if(userService.getLogInStatus()){
            if (userService.getRole() === 'maid')
                $state.transitionTo('view-maid-home',{maidId: userService.getUserId()});
            else if (userService.getRole() === 'maid-supervisor')
                $state.transitionTo('view-rooms-to-inspect');
        }
        //***********************************************************
        //Test variables to generate a list of assigned rooms faster

        //***********************************************************
        var maidRef = firebase.database().ref('hotels/' + userService.getHotelId()).child('/users').orderByChild('role').equalTo('maid');
        var maidRefArray = $firebaseArray(maidRef);
        maidRefArray.$loaded().then(function(maidArray){
            $scope.maidArray = maidArray;
        });
        var maidSupervisorRef = firebase.database().ref('hotels/' + userService.getHotelId()).child('/users').orderByChild('role').equalTo('maid-supervisor');
        var maidSupervisorRefArray = $firebaseArray(maidSupervisorRef);
        maidSupervisorRefArray.$loaded().then(function(maidSupervisorArray){
            $scope.maidSupervisorArray = maidSupervisorArray;
        });
        var managerRef = firebase.database().ref('hotels/' + userService.getHotelId()).child('/users').orderByChild('role').equalTo('manager');
        var managerRefArray = $firebaseArray(managerRef);
        managerRefArray.$loaded().then(function(managerArray){
            $scope.managerArray = managerArray;
        });

        $scope.userSelected = function (user) {
            userService.setUserIdRole(user.$id, user.name ,user.role);
        };



        // $scope.loginInfo = localStorage.getItem('id_token');
        // $scope.auth = authService;
        // $scope.send_info = function(){
        //     console.log($scope.username);
        //     firebase.auth().signInWithEmailAndPassword($scope.username, $scope.password).catch(function(error) {
        //         // Handle Errors here.
        //         var errorCode = error.code;
        //         var errorMessage = error.message;
        //         console.log(errorCode + ": " + errorMessage);
        //         // ...
        //     });
        // };


    }]);


})();